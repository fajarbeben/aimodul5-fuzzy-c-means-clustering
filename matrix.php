<!DOCTYPE html>
<html>
<head>
	<title>Matrix Uik</title>
</head>
<body>


<?php

	include ('rumus.php');

?>

<br>
Matrix Uik <br>
<table>
	<tr>
		<th class="kolom">
			i
		</th>

		<th class="kolom">
			k1
		</th>

		<th class="kolom">
			k2
		</th>

		<th class="kolom">
			K1^w
		</th>

		<th class="kolom">
			K2^W
		</th>
	</tr>

	<tr>
		<td class="kolom">
			1
		</td>

		<td class="kolom">
			<?php echo $k11; ?>
		</td>

		<td class="kolom">
			<?php echo $k21; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster11; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster21; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			2
		</td>

		<td class="kolom">
			<?php echo $k12; ?>
		</td>

		<td class="kolom">
			<?php echo $k22; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster12 ; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster22; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			3
		</td>

		<td class="kolom">
			<?php echo $k13; ?>
		</td>

		<td class="kolom">
			<?php echo $k23; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster13; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster23; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			4
		</td>

		<td class="kolom">
			<?php echo $k14; ?>
		</td>

		<td class="kolom">
			<?php echo $k24; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster14; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster24; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			5
		</td>

		<td class="kolom">
			<?php echo $k15; ?>
		</td>

		<td class="kolom">
			<?php echo $k25; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster15; ?>
		</td>

		<td class="kolom">
			<?php echo $pusatcluster25; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			
		</td>

		<td class="kolom">
			
		</td>

		<td class="kolom">
			
		</td>

		<td class="kolom">
			<b><?php echo $total_k1w; ?></b>
		</td>

		<td class="kolom">
			<b><?php echo $total_k2w; ?></b>
		</td>
	</tr>
</table>


<!-- pusat cluster A1 -->
 <br>
Pusat Cluster A1 <br>
<table border="1">
	<tr>
		<th class="kolom">
			K1^w*A1
		</th>
		<th class="kolom">
			K2^w*A1
		</th>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster11; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster21; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster12; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster22; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster13; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster23; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster14; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster24; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster15; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA1_pusatcluster25 ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<b><?php echo $totalA1_pusatcluster1 ?></b>
		</td>
		<td class="kolom">
			<b><?php echo $totalA1_pusatcluster2; ?></b>

		</td>
	</tr>
</table>

<!-- pusat cluster A2 -->
 <br>

Pusat Cluster A2 <br>
<table border="1">
	<tr>
		<th class="kolom">
			K1^w*A2
		</th>
		<th class="kolom">
			K2^w*A2
		</th>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster11; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster21; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster12; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster22; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster13; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster23; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster14; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster24; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster15; ?>
		</td>
		<td class="kolom">
			<?php echo $hasilA2_pusatcluster25 ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			<b><?php echo $totalA2_pusatcluster1 ?></b>
		</td>
		<td class="kolom">
			<b><?php echo $totalA2_pusatcluster2; ?></b>

		</td>
	</tr>
</table>

<br>
<!-- Class atrtribut -->
<table border = "1">
	<tr>
		<th class = "kolom">
			Class
		</th>
		<th class = "kolom">
			Atribut 1
		</th>
		<th class = "kolom">
			Atribut 2
		</th>
	</tr>

	<tr>
		<td class = "kolom">
			1
		</td>
		<td class = "kolom">
			<?php echo $classA11; ?>
		</td>
		<td class = "kolom">
			<?php echo $classA12; ?>
		</td>
	</tr>

	<tr>
		<td class = "kolom">
			2
		</td>
		<td class = "kolom">
			<?php echo $classA21; ?>
		</td>
		<td class = "kolom">
			<?php echo $classA22; ?>
		</td>
	</tr>
</table>

<br>
Hitung Jarak K baru dan P <br>

<!-- Cluster 1 -->
<table border="1">
	<tr>
		<th class="kolom">
			
		</th>
		<th class="kolom" colspan="2">
			Cluster 1
		</th>
		<th class="kolom">
			bobot
		</th>
		<th class="kolom">
			k1
		</th>
		<th class="kolom">
			Bobot * (k1^w)
		</th>
	</tr>

	<tr>
		<td class="kolom">
			1
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster411; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster412; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster11; ?>
		</td>
		<td class="kolom">
			<?php $cluster_k11 = $bobot_cluster11 + $bobot_cluster21;?>
			<?php $hasil_k11 = $bobot_cluster11 / $cluster_k11; ?>
			<?php echo $hasil_k11; ?>
		</td>
		<td class="kolom">
			<?php $bobotk11 = $bobot_cluster11 * $pusatcluster11; ?>
			<?php echo $bobotk11; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			2
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster421; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster422; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster12; ?>
		</td>
		<td class="kolom">
			<?php $cluster_k12 = $bobot_cluster12 + $bobot_cluster22; ?>
			<?php $hasil_k12 = $bobot_cluster12 / $cluster_k12; ?>
			<?php echo $hasil_k12; ?>
		</td>
		<td class="kolom">
			<?php $bobotk12 = $bobot_cluster12 * $pusatcluster12; ?>
			<?php echo $bobotk12; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			3
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster431; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster432; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster13; ?>
		</td>
		<td class="kolom">
			<?php $cluster_k13 = $bobot_cluster13 + $bobot_cluster23; ?>
			<?php $hasil_k13 = $bobot_cluster13 / $cluster_k13; ?>
			<?php echo $hasil_k13; ?>
		</td>
		<td class="kolom">
			<?php $bobotk13 = $bobot_cluster13 * $pusatcluster13; ?>
			<?php echo $bobotk13; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			4
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster441; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster442; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster14; ?>
		</td>
		<td class="kolom">
			<?php $cluster_k14 = $bobot_cluster14 + $bobot_cluster24; ?>
			<?php $hasil_k14 = $bobot_cluster14 /  $cluster_k14; ?>
			<?php echo $hasil_k14; ?>
		</td>
		<td class="kolom">
			<?php $bobotk14 = $bobot_cluster14 * $pusatcluster14; ?>
			<?php echo $bobotk14; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			5
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster451; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster452; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster15; ?>
		</td>
		<td class="kolom">
			<?php $cluster_k15 = $bobot_cluster15 + $bobot_cluster25; ?>
			<?php $hasil_k15 = $bobot_cluster15 / $cluster_k15; ?>
			<?php echo $hasil_k15; ?>
		</td>
		<td class="kolom">
			<?php $bobotk15 = $bobot_cluster15 * $pusatcluster15; ?>
			<?php echo $bobotk15; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			<?php $total_bobotk1w = $bobotk11 + $bobotk12 + $bobotk13 + $bobotk14 + $bobotk15; ?>
			<?php echo $total_bobotk1w; ?>
		</td>
	</tr>
</table>
<br>

<!-- Cluster 2 -->
<table border="1">
	<tr>
		<th class="kolom">
			
		</th>
		<th class="kolom" colspan="2">
			Cluster 2
		</th>
		<th class="kolom">
			bobot
		</th>
		<th class="kolom">
			k2
		</th>
		<th class="kolom">
			Bobot * (k2^w)
		</th>
	</tr>

	<tr>
		<td class="kolom">
			1
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2411; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2412; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster21; ?>
		</td>
		<td class="kolom">
			<?php $cluster_k21 = $bobot_cluster21 + $bobot_cluster11; ?>
			<?php $hasil_k21 = $bobot_cluster21 / $cluster_k21; ?>
			<?php echo $hasil_k21; ?>
		</td>
		<td class="kolom">
			<?php $bobotk21 = $bobot_cluster21 * $pusatcluster21; ?>
			<?php echo $bobotk21; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			2
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2421; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2422; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster22; ?>
		</td>
		<td class="kolom">
			<?php $cluster_k22 = $bobot_cluster22 + $bobot_cluster12; ?>
			<?php $hasil_k22 = $bobot_cluster22 / $cluster_k22; ?>
			<?php echo $hasil_k22; ?>
		</td>
		<td class="kolom">
			<?php $bobotk22 = $bobot_cluster22 * $pusatcluster22; ?>
			<?php echo $bobotk22; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			3
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2431; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2432; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster23; ?>
		</td>
		<td class="kolom">
			<?php $cluster_k23 = $bobot_cluster23 + $bobot_cluster13; ?>
			<?php $hasil_k23 = $bobot_cluster23 / $cluster_k23; ?>
			<?php echo $hasil_k23; ?>
		</td>
		<td class="kolom">
			<?php $bobotk23 = $bobot_cluster23 * $pusatcluster23; ?>
			<?php echo $bobotk23; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			4
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2441; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2442; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster24; ?>
		</td>
		<td class="kolom">
			<?php $cluster_k24 = $bobot_cluster24 + $bobot_cluster14; ?>
			<?php $hasil_k24 = $bobot_cluster24 / $cluster_k24; ?>
			<?php echo $hasil_k24; ?>
		</td>
		<td class="kolom">
			<?php $bobotk24 = $bobot_cluster24 * $pusatcluster24; ?>
			<?php echo $bobotk24; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			5
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2451; ?>
		</td>
		<td class="kolom">
			<?php echo $hasil_cluster2452; ?>
		</td>
		<td class="kolom">
			<?php echo $bobot_cluster25; ?>
			<?php  ?>
		</td>
		<td class="kolom">
			<?php $cluster_k25 = $bobot_cluster25 + $bobot_cluster15; ?>
			<?php $hasil_k25 = $bobot_cluster25 / $cluster_k25; ?>
			<?php echo $hasil_k25; ?>
		</td>
		<td class="kolom">
			<?php $bobotk25 = $bobot_cluster25 * $pusatcluster25; ?>
			<?php echo $bobotk25; ?>
		</td>
	</tr>

	<tr>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			
		</td>
		<td class="kolom">
			<?php $total_bobotk2w = $bobotk21 + $bobotk22 + $bobotk23 + $bobotk24 + $bobotk25; ?>
			<?php echo $total_bobotk2w; ?>
		</td>
	</tr>
</table>

</body>
</html>